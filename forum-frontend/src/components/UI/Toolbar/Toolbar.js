import React from 'react';
import {
    Collapse,
    Nav,
    Navbar,
    NavbarBrand, NavbarToggler,
} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="dark" dark expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Forum</NavbarBrand>
            <NavbarToggler  />
            <Collapse navbar>
                <Nav className="ml-auto" navbar>
                    {user ? <UserMenu logout={logout} user={user}/> : <AnonymousMenu/>}
                </Nav>
            </Collapse>
        </Navbar>
    );
};

export default Toolbar;