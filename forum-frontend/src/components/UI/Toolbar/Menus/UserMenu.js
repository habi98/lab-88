import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
    return (
      <Fragment>
          <NavItem>
              <NavLink tag={RouterNavLink} to="/new_post">Add new post</NavLink>
          </NavItem>
          <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                  {user.username}
              </DropdownToggle>
              <DropdownMenu right>
                  <DropdownItem onClick={logout}>
                     Logout
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                      Reset
                  </DropdownItem>
              </DropdownMenu>
          </UncontrolledDropdown>
      </Fragment>
    );
};

export default UserMenu;