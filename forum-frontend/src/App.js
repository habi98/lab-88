import React, {Component, Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch, withRouter} from "react-router-dom";
import Register from "./containers/Register/Register";
import Container from "reactstrap/es/Container";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import OnePost from "./containers/OnePost/OnePost";
import {connect} from "react-redux";
import AddNewPost from "./containers/AddNewPost/AddNewPost";
import {logoutUser} from "./store/actions/usersActions";
import {NotificationContainer} from "react-notifications";

class App extends Component{
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar logout={this.props.logoutUser} user={this.props.user}/>
                </header>
                <NotificationContainer/>
                <Container>
                    <Switch>
                        <Route path="/" exact component={Posts}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/register" component={Register}/>
                        <Route path="/post/:id" component={OnePost}/>
                        <Route path="/new_post" component={AddNewPost}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }


}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
