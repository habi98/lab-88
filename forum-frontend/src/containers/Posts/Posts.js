import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import {Card, CardBody} from "reactstrap";
import PostThumbnail from "../../components/PostsThumbnail/PostsThumbnail";
import {Link} from "react-router-dom";

class Posts extends Component {

    componentDidMount() {
        this.props.fetchPosts()
    }

    render() {
        return (
         <Fragment>
             <h2>Posts</h2>
             {this.props.posts && this.props.posts.map(post => (
                 <Card key={post._id} style={{marginBottom: '20px'}}>
                     <CardBody>
                         <PostThumbnail image={post.image}/>
                         <p>
                             <span className="text-muted mr-4">
                                     <span>{post.datetime} <b>{post.user.username}</b></span>
                                 </span>
                             <Link style={{display: 'block', padding: '10px 0'}} to={"/post/" + post._id}>
                                 {post.title}
                             </Link>
                         </p>

                     </CardBody>
                 </Card>
             ))}
         </Fragment>


        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts
});

const mapDispatchToProps = dispatch => ({
   fetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);