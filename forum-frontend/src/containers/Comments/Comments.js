import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardText} from "reactstrap";

class Comments extends Component {
    componentDidMount() {
        this.props.fetchComments(this.props.id)
    }

    render() {
        return (
         <Fragment>
             <div style={{padding: '20px 0'}}>
                 <h2>Comments</h2>
             </div>
             {this.props.comments.map(comment => (

                 <Card key={comment._id} className="mb-3">
                     <CardBody>
                         <CardText><strong>{comment.user.username}:</strong> {comment.text}</CardText>
                     </CardBody>
                 </Card>
             ))}
         </Fragment>

        );
    }
}

export default Comments;