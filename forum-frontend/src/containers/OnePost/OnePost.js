import React, {Component, Fragment} from 'react';
import {fetchOnePost} from "../../store/actions/postsActions";
import {connect} from "react-redux";
import imageNotAvailable from '../../assets/images/images.png';

import {Col, Row} from "reactstrap";
import {fetchComments} from "../../store/actions/commentsActions";
import Comments from "../Comments/Comments";
import FormAddComments from "../FormAddComments/FormAddComments";

class OnePost extends Component {
    componentDidMount() {
        const postId = this.props.match.params.id;

        this.props.fetchOnePost(postId);
    }

    render() {
        if (!this.props.post) return null;
        return (
           <Fragment>
               <h1>One post</h1>

               <Row className="border">
                   <Col sm={3}>
                           {this.props.post.image ? (
                               <img src={"http://localhost:8000/uploads/" + this.props.post.image}  alt ="post"/>
                           ): <img  src={imageNotAvailable} alt="post"/>}
                    </Col>
                       <Col sm={9}>
                           <h2>{this.props.post.user.username} <span>{this.props.post.datetime}</span></h2>
                           <p>{this.props.post.title}</p>
                           {this.props.post.description ? <span>{this.props.post.description}</span>: null}
                       </Col>
               </Row>
               <Comments
                   fetchComments={this.props.fetchComments}
                   id={this.props.match.params.id}
                   comments={this.props.comments}
               />
               <FormAddComments postId={this.props.match.params.id}/>
           </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    post: state.posts.post,
    comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    fetchOnePost: postId => dispatch(fetchOnePost(postId)),
    fetchComments: postId => dispatch(fetchComments(postId))
});

export default connect(mapStateToProps, mapDispatchToProps)(OnePost);