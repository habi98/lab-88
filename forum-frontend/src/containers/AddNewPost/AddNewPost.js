import React, {Component, Fragment} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {createPost} from "../../store/actions/postsActions";
import {connect} from "react-redux";

class AddNewPost extends Component {

    state = {
        title: '',
        description: '',
        image: ''
    };

    valueChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

       this.props.createPost(formData)
    };


    render() {
        return (
            <Fragment>
                <h2>Add new post </h2>
                {this.props.error && (
                    <Alert color="danger">
                        {this.props.error.error || this.props.error.global}
                    </Alert>
                )}
                <Form className="pt-5" onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="title"
                        title="Title"
                        type="text"
                        value={this.state.title}
                        onChange={this.valueChange}
                    />
                    <FormElement
                        propertyName="description"
                        title="Description"
                        type="textarea"
                        value={this.state.description}
                        onChange={this.valueChange}
                    />
                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        onChange={this.fileChangeHandler}
                    />

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button color="primary">Create post</Button>
                        </Col>
                    </FormGroup>

                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
     error: state.posts.createPostError
});

const mapDispatchToProps = dispatch => ({
    createPost: postData => dispatch(createPost(postData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPost);