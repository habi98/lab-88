import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";

class Register extends Component {
    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    getFieldError = fieldName  => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
    };

    submitFormHandler = event => {
       event.preventDefault()

       this.props.registerUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <h2>Register</h2>
                {this.props.error &&  this.props.error.global &&(
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        title="Username"
                        propertyName="username"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('username')}
                        placeholder="Enter username you registered with"
                        autoComplete="new-username"
                    />
                    <FormElement
                        title="Password"
                        propertyName="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('password')}
                        placeholder="Enter password"
                        autoComplete="new-password"
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="success">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Register);