import React, {Component, Fragment} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {createComment} from "../../store/actions/commentsActions";

class FormAddComments extends Component {
    state = {
        text: ''
    };

    inputChange = value => {
        this.setState({
           text: value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        const id = this.props.postId;
        this.props.createComment({text: this.state.text, post: this.props.postId}, id )
    };
    render() {
        if (!this.props.user)  return null;
        return (
       <Fragment>
           <Form onSubmit={this.submitFormHandler}>
               <FormElement
                   propertyName="text"
                   title="Text"
                   type="textarea"
                   value={this.state.text}
                   onChange={(e) => this.inputChange(e.target.value)}
               />


               <FormGroup row>
                   <Col sm={{offset: 2, size: 10}}>
                       <Button color="primary">Add comment</Button>
                   </Col>
               </FormGroup>
           </Form>
       </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    createComment: (data, id) => dispatch(createComment(data, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormAddComments);