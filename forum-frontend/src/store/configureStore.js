import {createStore, applyMiddleware, compose, combineReducers} from 'redux'
import thunkMiddleware from 'redux-thunk'

import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";


import {saveToLocalStorage, loadFromLocalStorage} from './localStorage'

import usersReducer from './reducers/usersReducer';
import postsReducer from './reducers/postsReducer'
import commentsReducer from './reducers/commentsReducer'

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    users: usersReducer,
    posts: postsReducer,
    comments: commentsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(thunkMiddleware, routerMiddleware(history)));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    })
});

export default store