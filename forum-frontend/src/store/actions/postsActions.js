import axios from '../../axios-forum';
import {push} from 'connected-react-router'

export const FETCH_POSTS_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_ONE_POST_SUCCESS = 'FETCH_ONE_POST_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchOnePostSuccess = post => ({type: FETCH_ONE_POST_SUCCESS, post});
export const createPostSuccess =()  => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = (error) => ({type: CREATE_POST_FAILURE, error});



export const fetchPosts = () => {
    return dispatch => {
        axios.get('/posts').then(response => {
            dispatch(fetchPostsSuccess(response.data))
        })
    }
};

export const fetchOnePost = (postId) => {
  return dispatch => {
      axios.get('/posts/' + postId).then(
          response => dispatch(fetchOnePostSuccess(response.data))
      )
  }
};

export const createPost = (postData) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if (!user) {
            dispatch(push('/login'));
        } else {
            axios.post('/posts', postData, {headers: {'Authorization': user.token}}).then(
                () => {
                    dispatch(createPostSuccess());
                    dispatch(push('/'))
                },
                error => {
                    if (error.response && error.response.data) {
                        dispatch(createPostFailure(error.response.data))
                    } else {
                        dispatch(createPostFailure({global: 'No connect'}))
                    }
                }
            )
        }
    }
};


