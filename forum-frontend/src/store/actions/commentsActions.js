import axios from '../../axios-forum';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});

export const fetchComments = postId => {
    return dispatch => {
        axios.get('/comments?post=' + postId).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};

export const createComment = (data, id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;

        if (!user) return null;

        axios.post('/comments', data, {headers: {'Authorization': user.token}}).then(
            () => {
                dispatch(createCommentSuccess());
                dispatch(fetchComments(id))
            }
        )
    }
};

