import {
    CREATE_POST_FAILURE,
    CREATE_POST_SUCCESS,
    FETCH_ONE_POST_SUCCESS,
    FETCH_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
    posts: [],
    post: null,
    createPostError: null,
};

const postsReducer = (state = initialState, action) => {
      switch (action.type) {
          case FETCH_POSTS_SUCCESS:
              return {...state, posts: action.posts};
          case FETCH_ONE_POST_SUCCESS:
              return {...state, post: action.post};
          case CREATE_POST_SUCCESS:
              return {...state, createPostError: null};
          case CREATE_POST_FAILURE:
              return {...state, createPostError: action.error};
          default:
              return state
      }
};


export default postsReducer