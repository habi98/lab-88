const mongoose = require('mongoose');
const config = require('./config');


const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;
    const collections = await connection.db.collections();

    for(let collection of collections) {
        await collection.drop()
    }

    await connection.close()

};

run().catch(error => {
    console.log('Something went wrong', error)
});
