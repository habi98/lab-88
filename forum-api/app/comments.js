const express =require('express');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');
const router = express.Router();



router.get('/', (req, res) => {
    let query;

    if (req.query.post) {
        query = {post: req.query.post}
    }
    Comment.find(query).populate('user')
        .then(comments => res.send(comments))
        .catch(() => res.sendStatus(500))
});

router.post('/', auth, (req, res) => {
   const commentData = req.body;


   const comment = new Comment(commentData);

   comment.user = req.user._id;

   comment.save()
       .then(result => res.send(result))
       .catch(error => res.sendStatus(400).send(error))
});

module.exports = router;