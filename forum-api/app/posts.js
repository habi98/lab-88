const express =require('express');
const auth = require('../middleware/auth');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');
const Post = require('../models/Post');
const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


router.get('/',  (req, res) => {

    Post.find().sort({datetime: -1}).populate('user')
        .then(posts => res.send(posts))
        .catch(() => res.sendStatus(500))
});

router.get('/:id',  (req, res) => {

    Post.findById(req.params.id).populate('user')
        .then(post => res.send(post))
        .catch(() => res.sendStatus(500))
});

router.post('/', upload.single('image'), auth, (req, res) => {

    if (req.file) {
        req.body.image = req.file.filename
    }

    if(!(req.body.description || req.body.image)) return res.status(400).send({error: 'description or image field must be filled'});

    const post = new Post(req.body);

   post.user = req.user._id;
   post.datetime = new Date().toISOString();

   post.save()
       .then(result => res.send(result))
       .catch(() => res.sendStatus(500))

});




module.exports = router;